﻿using System;

namespace Database.Configuration
{
    public class DatabaseConfiguration : IDatabaseConfiguration
    {
        public DatabaseConnectionString Auth { get; set; }
        public DatabaseConnectionString Character { get; set; }
        public DatabaseConnectionString World { get; set; }

        public IDatabaseConnectionString GetConnectionString(DatabaseType type)
        {
            switch (type)
            {
                default:
                case DatabaseType.Auth:
                    return Auth;
                case DatabaseType.Character:
                    return Character;
                case DatabaseType.World:
                    return World;
            }
        }
    }
}