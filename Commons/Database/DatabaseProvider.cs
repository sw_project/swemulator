﻿namespace Database.Configuration
{
    public enum DatabaseProvider
    {
        MySql,
        MsSql
    }
}