﻿namespace Database.Configuration
{
    public enum DatabaseType
    {
        Auth,
        Character,
        World
    }
}