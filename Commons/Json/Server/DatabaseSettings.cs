﻿namespace Commons.Json.Server
{
    public class DatabaseSettings
    {
        public string Auth { get; set; }
        public string Character { get; set; }
        public string World { get; set; }
    }
}