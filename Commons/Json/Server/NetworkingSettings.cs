﻿namespace Commons.Json.Server
{
    public class NetworkingSettings
    {
        public int Port { get; set; }
        public int BufferSize { get; set; }
        public int Boss { get; set; }
        public int Worker { get; set; }
        public int Backlog { get; set; }
    }
}