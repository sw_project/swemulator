﻿using System.IO;
using Newtonsoft.Json;

namespace Commons.Json
{
    public class DeserializeFile<T>
    {
        public T Build(string path)
        {
            return JsonConvert.DeserializeObject<T>(File.ReadAllText(path));
        }
    }
}