﻿namespace Commons.Utils
{
    public static class ServerStatus
    {
        public static int Get(int count, int limit)
        {
            var percentage = (int) (0.5f + 100f * count / limit);
            return percentage >= 100 ? 4 : percentage >= 66.66 ? 3 : percentage >= 33.33 ? 2 : 1;
        }
    }
}