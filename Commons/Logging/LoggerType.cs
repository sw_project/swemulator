﻿namespace Commons.Logging
{
    public enum LoggerType
    {
        Info,
        Debug,
        Warn,
        Alert,
        Error,
        Write
    }
}