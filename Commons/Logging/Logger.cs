﻿namespace Commons.Logging
{
    public class Logger
    {
        public Logger(LoggerLocation location, string name)
        {
            Location = location;
            Name = name;
        }

        private LoggerLocation Location { get; }
        private string Name { get; }

        public void Info(string message)
        {
            LogManager.Enqueue(new LoggerMessage(Location, Name, message, LoggerType.Info));
        }

        public void Debug(string message)
        {
            LogManager.Enqueue(new LoggerMessage(Location, Name, message, LoggerType.Debug));
        }

        public void Warn(string message)
        {
            LogManager.Enqueue(new LoggerMessage(Location, Name, message, LoggerType.Warn));
        }

        public void Alert(string message)
        {
            LogManager.Enqueue(new LoggerMessage(Location, Name, message, LoggerType.Alert));
        }

        public void Error(string message)
        {
            LogManager.Enqueue(new LoggerMessage(Location, Name, message, LoggerType.Error));
        }

        public void Write(string message)
        {
            LogManager.Enqueue(new LoggerMessage(Location, Name, message, LoggerType.Write));
        }
    }
}