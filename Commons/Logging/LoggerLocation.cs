﻿namespace Commons.Logging
{
    public enum LoggerLocation
    {
        Center,
        Lua,
        Database,
        Commons,
        Networking,
        Game
    }
}