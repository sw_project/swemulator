﻿using Database.Configuration;
using Database.Extensions;
using Database.Models;
using Microsoft.EntityFrameworkCore;

namespace Database
{
    public sealed class AuthContext : DbContext
    {
        public DbSet<UserModel> Users { get; set; }
        public DbSet<UserCharacterModel> UserCharacters { get; set; }
        public DbSet<ServerModel> Servers { get; set; }
        
        private readonly IDatabaseConfiguration configuration;

        public AuthContext(IDatabaseConfiguration configuration)
        {
            this.configuration = configuration;
        }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseConfiguration(configuration, DatabaseType.Auth);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<UserModel>(e =>
            {
                e.ToTable("users");

                e.HasKey(m => m.Id);

                e.HasIndex(m => m.Username)
                    .IsUnique();
                e.HasIndex(m => new { m.Id, m.SessionKey });

                e.Property(m => m.Username)
                    .IsRequired()
                    .HasMaxLength(32);
                e.Property(m => m.Password)
                    .IsRequired()
                    .HasMaxLength(128);
                e.Property(m => m.DateCreated)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");
            });
            
            modelBuilder.Entity<UserCharacterModel>(e =>
            {
                e.ToTable("users_characters");

                e.HasKey(m => m.Id);

                e.HasOne(m => m.Server)
                    .WithMany(m => m.Characters)
                    .HasForeignKey(m => m.ServerId);
            });
            
            modelBuilder.Entity<ServerModel>(e =>
            {
                e.ToTable("servers");

                e.HasKey(m => m.Id);

                e.Property(m => m.Name)
                    .IsRequired()
                    .HasMaxLength(32);
                e.Property(m => m.Address)
                    .IsRequired()
                    .HasMaxLength(64);

                // seeded data
                e.HasData(new ServerModel
                {
                    Id   = 1,
                    Name = "Local",
                    Address = "127.0.0.1",
                    Port = 10100,
                });
            });
        }
    }
}