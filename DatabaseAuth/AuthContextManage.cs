﻿using Database.Configuration;
using Microsoft.EntityFrameworkCore;

namespace Database
{
    public class AuthContextManage
    {
        private static IDatabaseConfiguration DatabaseConfiguration { get; set; }
        public static AuthContext Context => new AuthContext(DatabaseConfiguration);
        public static void Build(string connectionString)
        {
            DatabaseConfiguration = new DatabaseConfiguration
            {
                Auth = new DatabaseConnectionString
                {
                    Provider = DatabaseProvider.MySql,
                    ConnectionString = connectionString
                }
            };
        }
        
        public static void Migrate()
        {
            Context.Database.Migrate();
        }
    }
}