﻿using Database.Configuration;
using Database.Extensions;
using Database.Models.Character;
using Microsoft.EntityFrameworkCore;

namespace Database
{
    public sealed class CharacterContext : DbContext
    {
        public DbSet<CharacterModel> Characters { get; set; }
        
        private readonly IDatabaseConfiguration configuration;

        public CharacterContext(IDatabaseConfiguration configuration)
        {
            this.configuration = configuration;
        }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseConfiguration(configuration, DatabaseType.Character);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<CharacterModel>(e =>
            {
                e.ToTable("characters");

                e.HasKey(m => m.Id);

                e.Property(m => m.UserId).IsRequired().HasMaxLength(32);

                e.HasIndex(m => m.Name).IsUnique();

                e.Property(m => m.Type).IsRequired();

                e.Property(m => m.HairStyle).IsRequired();

                e.Property(m => m.HairColor).IsRequired();

                e.Property(m => m.EyeColor).IsRequired();

                e.Property(m => m.SkinColor).IsRequired();

                e.Property(m => m.Level).IsRequired().HasDefaultValue(1);

                e.Property(m => m.WeaponId).IsRequired().HasDefaultValue(0);

                e.Property(m => m.TitleA).IsRequired().HasDefaultValue(0);

                e.Property(m => m.TitleB).IsRequired().HasDefaultValue(0);

                e.Property(m => m.GuildId).IsRequired().HasDefaultValue(0);

                e.Property(m => m.GuildName).IsRequired();

                e.Property(m => m.HP).IsRequired().HasDefaultValue(100);

                e.Property(m => m.HPBase).IsRequired().HasDefaultValue(100);

                e.Property(m => m.Energy).IsRequired().HasDefaultValue(300);

                e.Property(m => m.EnergyExtra).IsRequired().HasDefaultValue(300);

                e.Property(m => m.EXP).IsRequired().HasDefaultValue(0);

                e.Property(m => m.Zenny).IsRequired().HasDefaultValue(50);

                e.Property(m => m.BP).IsRequired().HasDefaultValue(0);

                e.Property(m => m.Ether).IsRequired().HasDefaultValue(0);

                e.Property(m => m.Map).IsRequired().HasDefaultValue(10003);

                e.Property(m => m.X).IsRequired().HasDefaultValue(10000);

                e.Property(m => m.Y).IsRequired().HasDefaultValue(1000);

                e.Property(m => m.Z).IsRequired().HasDefaultValue(10000);

                e.Property(m => m.Rotation).IsRequired().HasDefaultValue(0);

                e.Property(m => m.DateCreated)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");
            });
        }
    }
}