﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DatabaseCharacter.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "characters",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<int>(maxLength: 32, nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    HairStyle = table.Column<int>(nullable: false),
                    HairColor = table.Column<int>(nullable: false),
                    EyeColor = table.Column<int>(nullable: false),
                    SkinColor = table.Column<int>(nullable: false),
                    Level = table.Column<int>(nullable: false, defaultValue: 1),
                    WeaponId = table.Column<int>(nullable: false, defaultValue: 0),
                    TitleA = table.Column<int>(nullable: false, defaultValue: 0),
                    TitleB = table.Column<int>(nullable: false, defaultValue: 0),
                    GuildId = table.Column<int>(nullable: false, defaultValue: 0),
                    GuildName = table.Column<string>(nullable: false),
                    HP = table.Column<int>(nullable: false, defaultValue: 100),
                    HPBase = table.Column<int>(nullable: false, defaultValue: 100),
                    Energy = table.Column<int>(nullable: false, defaultValue: 300),
                    EnergyExtra = table.Column<int>(nullable: false, defaultValue: 300),
                    EXP = table.Column<int>(nullable: false, defaultValue: 0),
                    Zenny = table.Column<int>(nullable: false, defaultValue: 50),
                    BP = table.Column<int>(nullable: false, defaultValue: 0),
                    Ether = table.Column<int>(nullable: false, defaultValue: 0),
                    Map = table.Column<int>(nullable: false, defaultValue: 10003),
                    X = table.Column<int>(nullable: false, defaultValue: 10000),
                    Y = table.Column<int>(nullable: false, defaultValue: 1000),
                    Z = table.Column<int>(nullable: false, defaultValue: 10000),
                    Rotation = table.Column<int>(nullable: false, defaultValue: 0),
                    Slot = table.Column<int>(nullable: false),
                    DateCreated = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "CURRENT_TIMESTAMP")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_characters", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_characters_Name",
                table: "characters",
                column: "Name",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "characters");
        }
    }
}
