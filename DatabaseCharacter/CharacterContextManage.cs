﻿using Database.Configuration;
using Microsoft.EntityFrameworkCore;

namespace Database
{
    public class CharacterContextManage
    {
        private static IDatabaseConfiguration DatabaseConfiguration { get; set; }
        public static CharacterContext Context => new CharacterContext(DatabaseConfiguration);
        public static void Build(string connectionString)
        {
            DatabaseConfiguration = new DatabaseConfiguration
            {
                Character = new DatabaseConnectionString
                {
                    Provider = DatabaseProvider.MySql,
                    ConnectionString = connectionString
                }
            };
        }
        
        public static void Migrate()
        {
            Context.Database.Migrate();
        }
    }
}