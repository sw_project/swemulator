﻿using Database.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Design
{
    public class CharacterContextFactory : IDesignTimeDbContextFactory<CharacterContext>
    {
        public CharacterContext CreateDbContext(string[] args)
        {
            return new CharacterContext(new DatabaseConfiguration
            {
                Character = new DatabaseConnectionString
                {
                    Provider = DatabaseProvider.MySql,
                    ConnectionString = "Server=localhost;Database=soulworker_character;username=root;password=123456"
                }
            });
        }
    }
}
