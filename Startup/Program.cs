﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Startup
{
    class Program
    {
        private static IList<Process> processes = new List<Process>();
        private static IList<ProcessStartInfo> processesInfo = new List<ProcessStartInfo>();
        static void Main(string[] args)
        {
            Console.Title = "SoulWorker Server Manager";
            RunAsync().GetAwaiter().GetResult();
        }
        private static async Task RunAsync()
        {

            var process = Process.Start(CreateProcessInfo("C:\\Users\\Thalys\\Documents\\gitlab\\swemulator\\Startup\\bin\\Debug\\center\\Center.exe"));
            process.Refresh();

            await Task.Delay(-1);
        }
        private static ProcessStartInfo CreateProcessInfo(string path, string args = null)
        {
            Console.WriteLine(Path.GetDirectoryName(path));
            var processStartInfo = new ProcessStartInfo
            {
                WorkingDirectory = Path.GetDirectoryName(path),
                FileName = path,
                Arguments = args,
                WindowStyle = ProcessWindowStyle.Normal,
                UseShellExecute = true
            };
            return processStartInfo;
        }
    }
}
