﻿using System.Collections.Generic;
using System.IO;
using DotNetty.Buffers;
using DotNetty.Codecs;
using DotNetty.Transport.Channels;
using Networking.Crypt;
using Networking.Messages;

namespace Networking.Codec
{
    public class PacketDecoder : MessageToMessageDecoder<IByteBuffer>
    {
        protected override void Decode(IChannelHandlerContext context, IByteBuffer message, List<object> output)
        {
            var dataReceived = new byte[message.ReadableBytes];
            message.ReadBytes(dataReceived);
            using (var ms = new MemoryStream(dataReceived))
            using (var br = new BinaryReader(ms))
            {
                var keyIdentifier = br.ReadInt16();
                var size = br.ReadInt16();
                var sender = br.ReadByte();
                Cryptography.CryptBytes(keyIdentifier, ref dataReceived, 5); // 5 = 2 (short) + 2 (short) + 1 (byte)
                var code1 = br.ReadByte();
                var code2 = br.ReadByte();
                var code = code2 + (code1 << 8);
                var packetData = br.ReadBytes(size - 7);
                output.Add(new InMessage((ushort) code, size, sender, packetData));
            }
        }
    }
}