﻿using System;
using System.IO;
using Commons.Utils;
using DotNetty.Buffers;
using DotNetty.Codecs;
using DotNetty.Transport.Channels;
using Networking.Crypt;
using Networking.Extensions;
using Networking.Messages;

namespace Networking.Codec
{
    public class PacketEncoder : MessageToByteEncoder<OutMessage>
    {
        protected override void Encode(IChannelHandlerContext context, OutMessage message, IByteBuffer output)
        {
            var buffer = message.Buffer;
            var packetSize = buffer.Length + 5;
            using (var ms = new MemoryStream(packetSize))
            using (var br = new BinaryWriter(ms))
            {
                br.Write((short) 2);
                br.Write((short) packetSize);
                br.Write((byte) 1);
                Console.WriteLine("===================================================================");
                Console.WriteLine($" send: \n {buffer.ToHex()}");
                Console.WriteLine("===================================================================");
                br.Write(Cryptography.CryptBytes(2, buffer));
                output.WriteBytes(ms.ToArray());
            }
        }
    }
}