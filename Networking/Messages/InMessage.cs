﻿namespace Networking.Messages
{
    public class InMessage
    {
        public InMessage(ushort code, short size, byte sender, byte[] buffer)
        {
            Code = code;
            Size = size;
            Sender = sender;
            Buffer = buffer;
        }

        public ushort Code { get; }
        public short Size { get; }
        public byte Sender { get; }
        public byte[] Buffer { get; }
    }
}