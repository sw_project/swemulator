﻿using System;
using System.IO;
using Networking.Extensions;

namespace Networking.Messages
{
    public class OutMessage : IDisposable
    {
        private readonly BinaryWriter _binaryWriter;
        private readonly MemoryStream _memoryStream;

        protected OutMessage(Opcodes code)
        {
            _memoryStream = new MemoryStream();
            _binaryWriter = new BinaryWriter(_memoryStream);
            _binaryWriter.Write((ushort) code, true);
        }

        public byte[] Buffer => _memoryStream.ToArray();

        public void Dispose()
        {
            _memoryStream?.Dispose();
            _binaryWriter?.Dispose();
        }

        protected void WriteByteLoop(byte value, int count)
        {
            for (var i = 0; i < count; i++)
            {
                _binaryWriter.Write(value);
            }
        }

        protected void Write(byte[] value)
        {
            _binaryWriter.Write(value);
        }

        protected void WriteInt8(byte value)
        {
            _binaryWriter.Write(value);
        }

        protected void WriteSInt8(sbyte value)
        {
            _binaryWriter.Write(value);
        }

        protected void WriteInt16(short value)
        {
            _binaryWriter.Write(value);
        }

        protected void WriteUInt16(ushort value)
        {
            _binaryWriter.Write(value);
        }

        protected void WriteInt32(int value)
        {
            _binaryWriter.Write(value);
        }

        protected void WriteUInt32(uint value)
        {
            _binaryWriter.Write(value);
        }

        protected void WriteInt64(long value)
        {
            _binaryWriter.Write(value);
        }

        protected void WriteUInt64(ulong value)
        {
            _binaryWriter.Write(value);
        }

        protected void WriteSingle(float value)
        {
            _binaryWriter.Write(value);
        }

        protected void WriteBoolean(bool boolean)
        {
            _binaryWriter.Write(boolean);
        }

        protected void WriteUnicodeString(string data)
        {
            _binaryWriter.WriteUnicodeString(data);
        }

        protected void WriteAsciiString(string data)
        {
            _binaryWriter.WriteAsciiString(data);
        }

        protected void WriteUTF8String(string data)
        {
            _binaryWriter.WriteUTF8String(data);
        }
    }
}