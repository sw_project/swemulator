﻿using System.Net;
using Database.Models;
using DotNetty.Transport.Channels;
using Networking.Messages;

namespace Networking.Sessions
{
    public class Session
    {
        public Session(IChannel channel)
        {
            Channel = channel;
        }
        public UserModel Model { get; set; }
        private IChannel Channel { get; }
        public string Address => ((IPEndPoint) Channel.RemoteAddress).Address.MapToIPv4().ToString();

        public async void Send(OutMessage packetOut)
        {
            if (!Channel.IsWritable) return;
            await Channel.WriteAndFlushAsync(packetOut);
        }

        public async void Disconnect()
        {
            await Channel.DisconnectAsync();
        }
    }
}