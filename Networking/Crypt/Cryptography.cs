﻿using System;
using System.IO;
using System.Security.Cryptography;
using Commons.Logging;

namespace Networking.Crypt
{
    public static class Cryptography
    {
        private static readonly Logger Logger = LogManager.Instance(LoggerLocation.Networking, "Cryptography");
        private static byte[] KeyTable { get; set; }

        public static void Load(string filePath)
        {
            Logger.Info("Loading cryptography...");
            KeyTable = File.ReadAllBytes(filePath);

            if (KeyTable.Length < 64)
                Logger.Alert($"Key table has an strange length ({KeyTable.Length})!");
            else
                Logger.Debug($"Key table length ({KeyTable.Length})");
        }

        public static byte[] CryptBytes(short keyIdentifier, byte[] bytes)
        {
            for (var i = 0; i < bytes.Length; i++)
                bytes[i] ^= KeyTable[4 * keyIdentifier - 3 * (int) ((float) i / 3) + i];
            return bytes;
        }

        public static void CryptBytes(short keyIdentifier, ref byte[] bytes, int offset)
        {
            try
            {
                for (var i = 0; i < bytes.Length - offset; i++)
                    bytes[offset + i] ^= KeyTable[4 * keyIdentifier - 3 * (int)((float)i / 3) + i];
            } catch (Exception)
            {

            }
        }
        
        public static ulong GenerateSessionKey()
        {
            byte[] GenerateBytes(uint amount)
            {
                using (var provider = new RNGCryptoServiceProvider())
                {
                    var bytes = new byte[amount];
                    provider.GetBytes(bytes);
                    return bytes;
                }
            }
            return BitConverter.ToUInt64(GenerateBytes(sizeof(ulong)));
        }
    }
}