﻿using System;
using System.Net;
using System.Threading.Tasks;
using Commons.Json.Server;
using Commons.Logging;
using DotNetty.Transport.Bootstrapping;
using DotNetty.Transport.Channels;
using DotNetty.Transport.Channels.Sockets;
using Networking.Codec;
using Networking.Handler;
using Networking.Sessions;

namespace Networking
{
    public class Bootstrap
    {
        private readonly Logger Logger = LogManager.Instance(LoggerLocation.Networking, "Bootstrap");

        public async Task Bind(IServerHandler serverHandler, NetworkingSettings settings)
        {
            var bossGroup = new MultithreadEventLoopGroup(settings.Boss);
            Logger.Debug($"Boss group event loop count: {settings.Boss}");
            var workerGroup = new MultithreadEventLoopGroup(settings.Worker);
            Logger.Debug($"Worker group event loop count: {settings.Worker}");
            var bootstrap = new ServerBootstrap()
                .Group(bossGroup, workerGroup)
                .Channel<TcpServerSocketChannel>()
                .ChildHandler(new ActionChannelInitializer<ISocketChannel>(channel =>
                {
                    Logger.Info("Bootstrap Channel Initialized");
                    channel.Pipeline.AddLast(new PacketEncoder());
                    channel.Pipeline.AddLast(new PacketDecoder());
                    channel.Pipeline.AddLast(new Handler.Handler(new Session(channel), serverHandler));
                }))
                .ChildOption(ChannelOption.AutoRead, true)
                .ChildOption(ChannelOption.SoKeepalive, true)
                .ChildOption(ChannelOption.TcpNodelay, true)
                .ChildOption(ChannelOption.SoBacklog, settings.Backlog)
                .ChildOption(ChannelOption.SoRcvbuf, settings.BufferSize)
                .ChildOption(ChannelOption.SoSndbuf, settings.BufferSize);
            await bootstrap.BindAsync(IPAddress.Any, settings.Port);
            Logger.Info($"Server working on port: {settings.Port}");
        }
    }
}