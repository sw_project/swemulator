﻿using System;
using Commons.Logging;
using DotNetty.Transport.Channels;
using Networking.Messages;
using Networking.Sessions;

namespace Networking.Handler
{
    public class Handler : ChannelHandlerAdapter
    {
        private static readonly Logger Logger = LogManager.Instance(LoggerLocation.Networking, "Handler");

        public Handler(Session session, IServerHandler serverHandler)
        {
            Session = session;
            ServerHandler = serverHandler;
        }

        public Session Session { get; }
        public IServerHandler ServerHandler { get; }

        public override void ChannelActive(IChannelHandlerContext context)
        {
            Logger.Info("Channel Active");
            if (SessionManager.Add(context, Session)) ServerHandler.ChannelActive(Session);
        }

        public override void ChannelInactive(IChannelHandlerContext context)
        {
            ServerHandler.ChannelInactive(Session);
            if (!SessionManager.Remove(context)) Logger.Warn($"Can't remove a session: {Session.Address}");
        }

        public override void ChannelRead(IChannelHandlerContext ctx, object output)
        {
            if (!(output is InMessage message)) return;
            ServerHandler.ChannelRead(Session, message.Code, message.Sender, message.Buffer);
        }

        public override void ExceptionCaught(IChannelHandlerContext context, Exception exception)
        {
            Logger.Error($"{exception.Message}{exception.StackTrace}");
        }
    }
}