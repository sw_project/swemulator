﻿using Networking.Sessions;

namespace Networking.Handler
{
    public interface IServerHandler
    {
        void ChannelActive(Session session);
        void ChannelInactive(Session session);
        void ChannelRead(Session session, ushort packetId, byte sender, byte[] buffer);
    }
}