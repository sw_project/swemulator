﻿using System.IO;
using Networking.Sessions;

namespace Networking.Handler
{
    public interface IPacketHandler
    {
        void Dispatch(byte sender, BinaryReader binaryReader, Session session);
    }
}