﻿using System.IO;
using System.Text;

namespace Networking.Extensions
{
    public static class BinaryReaderExtensions
    {
        public static string ReadUnicodeString(this BinaryReader reader)
        {
            var length = reader.ReadInt16();
            var buffer = reader.ReadBytes(length);
            return Encoding.Unicode.GetString(buffer).TrimEnd('\0');
        }
    }
}