﻿using System;
using System.IO;
using System.Text;

namespace Networking.Extensions
{
    public static class BinaryWriterExtensions
    {
        public static void Write(this BinaryWriter writer, ushort int16, bool reverse = false)
        {
            writer.Write(BitConverter.GetBytes(reverse ? (ushort) ((int16 << 8) + (int16 >> 8)) : int16));
        }

        public static void WriteUnicodeString(this BinaryWriter writer, string value)
        {
            var bytesOfString = Encoding.Unicode.GetBytes(value);
            var length = (ushort) bytesOfString.Length;
            writer.Write(length);
            writer.Write(bytesOfString);

            if (bytesOfString.Length < length) writer.Write(new byte[length - bytesOfString.Length]);
        }

        public static void WriteAsciiString(this BinaryWriter writer, string value)
        {
            var bytesOfString = Encoding.ASCII.GetBytes(value);
            var length = (ushort) bytesOfString.Length;
            writer.Write(length);
            writer.Write(bytesOfString);

            if (bytesOfString.Length < length) writer.Write(new byte[length - bytesOfString.Length]);
        }

        public static void WriteUTF8String(this BinaryWriter writer, string value)
        {
            var bytesOfString = Encoding.UTF8.GetBytes(value);
            var length = (ushort) bytesOfString.Length;
            writer.Write(length);
            writer.Write(bytesOfString);

            if (bytesOfString.Length < length) writer.Write(new byte[length - bytesOfString.Length]);
        }
    }
}