﻿using System.Collections.Concurrent;
using DotNetty.Transport.Channels;
using Networking.Sessions;

namespace Networking
{
    internal static class SessionManager
    {
        private static readonly ConcurrentDictionary<IChannelId, Session> Sessions =
            new ConcurrentDictionary<IChannelId, Session>();

        internal static bool Add(IChannelHandlerContext context, Session session)
        {
            return !Sessions.ContainsKey(context.Channel.Id) && Sessions.TryAdd(context.Channel.Id, session);
        }

        internal static bool Remove(IChannelHandlerContext context)
        {
            return Sessions.ContainsKey(context.Channel.Id) && Sessions.TryRemove(context.Channel.Id, out var session);
        }
    }
}