﻿using System;

namespace Networking.Attributes
{
    public class CodeInAttribute : Attribute
    {
        public CodeInAttribute(ushort code, bool enter = false)
        {
            Code = code;
            Enter = enter;
        }

        public CodeInAttribute(object code, bool enter = false)
        {
            Code = (ushort)code;
            Enter = enter;

        }

        public ushort Code { get; }

        public bool Enter { get; }
    }
}