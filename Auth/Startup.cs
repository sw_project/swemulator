﻿using System.Threading.Tasks;
using Center.Communication;
using Commons.Json;
using Commons.Json.Server;
using Commons.Logging;
using Database;
using Networking;
using Networking.Crypt;

namespace Center
{
    internal class Startup
    {
        private static readonly Logger Logger = LogManager.Instance(LoggerLocation.Center, "Startup");
        private static NetworkingSettings NetworkingSettings { get; set; }
        private static DatabaseSettings DatabaseSettings { get; set; }

        private static void Main(string[] args)
        {
            Logger.Info("Starting auth server...");
            NetworkingSettings = new DeserializeFile<NetworkingSettings>().Build(@"data\networking.json");
            DatabaseSettings = new DeserializeFile<DatabaseSettings>().Build(@"data\database.json");
            Cryptography.Load("data/key_table");
            PacketProcessor.Initialize();
            AuthContextManage.Build(DatabaseSettings.Auth);
            AuthContextManage.Migrate();
            RunAsync().GetAwaiter().GetResult();
        }

        private static async Task RunAsync()
        {
            var bootstrap = new Bootstrap();
            await bootstrap.Bind(new ServerHandler(), NetworkingSettings);
            await Task.Delay(-1);
        }
    }
}