﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Commons.Logging;
using Networking.Attributes;
using Networking.Handler;

namespace Center.Communication
{
    public static class PacketProcessor
    {
        private static readonly Logger Logger = LogManager.Instance(LoggerLocation.Center, "PacketProcessor");

        private static readonly Dictionary<ushort, IPacketHandler> Packets = new Dictionary<ushort, IPacketHandler>();

        public static bool TryGetPacket(ushort code, out IPacketHandler packetHandler)
        {
            return Packets.TryGetValue(code, out packetHandler);
        }

        public static void Initialize()
        {
            foreach (var type in Assembly.GetExecutingAssembly().GetTypes())
            {
                if (!type.IsClass || type.GetInterface(typeof(IPacketHandler).FullName) == null) continue;

                var packetAttribute =
                    (CodeInAttribute[]) type.GetCustomAttributes(typeof(CodeInAttribute), true);
                if (packetAttribute.Length <= 0) continue;
                if (Packets.ContainsKey(packetAttribute[0].Code))
                {
                    Logger.Error($"Packet {nameof(PacketProcessor)} is repeating the code of other packet.");
                    continue;
                }
                var request = (IPacketHandler)Activator.CreateInstance(type);
                Packets.Add(packetAttribute[0].Code, request);
            }

            if (Packets.Count == 0)
            {
                Logger.Warn("No packets has been found, the server will be terminated.");
                Console.ReadKey(true);
                Environment.Exit(1);
                return;
            }

            Logger.Info($"{Packets.Count} packets loaded.");
        }
    }
}