﻿using System;
using System.IO;
using System.Linq;
using Center.Communication.Messages.Output;
using Database;
using Networking;
using Networking.Attributes;
using Networking.Crypt;
using Networking.Extensions;
using Networking.Handler;
using Networking.Sessions;

namespace Center.Communication.Messages.Input
{
    [CodeIn(Opcodes.ClientLoginReq)]
    public class ClientLoginReq : IPacketHandler
    {
        public void Dispatch(byte sender, BinaryReader binaryReader, Session session)
        {
            var username = binaryReader.ReadUnicodeString();
            var password = binaryReader.ReadUnicodeString();
            var mac = binaryReader.ReadUnicodeString();
            var ver = binaryReader.ReadInt32();
            using (var context = AuthContextManage.Context)
            {
                var userModel = context.Users.FirstOrDefault(x => x.Username == username && x.Password == password);
                if (userModel == null)
                {
                    session.Send(new ClientLoginResult(errorCode: 1));
                    return;
                }
                userModel.LastMac = mac;
                userModel.SessionKey = Cryptography.GenerateSessionKey();
                context.Users.Update(userModel);
                context.SaveChanges();
                session.Send(new ClientLoginResult(mac: mac, accountId: (uint)userModel.Id, sessionKey: userModel.SessionKey));
                session.Model = userModel;

                Console.WriteLine($"{username} : {password}");
            }
        }
    }
}