﻿using System.IO;
using System.Linq;
using Center.Communication.Messages.Output;
using Database;
using Networking;
using Networking.Attributes;
using Networking.Handler;
using Networking.Sessions;

namespace Center.Communication.Messages.Input
{
    [CodeIn(Opcodes.ClientServerListReq)]
    public class ClientServerListReq : IPacketHandler
    {
        public void Dispatch(byte sender, BinaryReader binaryReader, Session session)
        {
            var userId = binaryReader.ReadInt32();
            if (session.Model == null) return;
            using (var context = AuthContextManage.Context)
            {
                var servers = context.Servers.ToList();
                session.Send(new ClientServerListSend(servers, session.Model.Characters));
                session.Send(new ClientOptionLoad(userId));
            }
        }
    }
}