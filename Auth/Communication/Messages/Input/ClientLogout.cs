﻿using System.IO;
using Networking;
using Networking.Attributes;
using Networking.Handler;
using Networking.Sessions;

namespace Center.Communication.Messages.Input
{
    [CodeIn(Opcodes.ClientLogout)]
    public class ClientLogout : IPacketHandler
    {
        public void Dispatch(byte sender, BinaryReader binaryReader, Session session)
        {
            session.Disconnect();
        }
    }
}