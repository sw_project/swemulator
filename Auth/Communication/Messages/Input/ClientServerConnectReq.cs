﻿using System;
using System.IO;
using System.Linq;
using Center.Communication.Messages.Output;
using Database;
using Networking;
using Networking.Attributes;
using Networking.Handler;
using Networking.Messages;
using Networking.Sessions;

namespace Center.Communication.Messages.Input
{
    [CodeIn(Opcodes.ClientServerConnectReq)]
    public class ClientServerConnectReq : IPacketHandler
    {
        public void Dispatch(byte sender, BinaryReader binaryReader, Session session)
        {
            var serverId = binaryReader.Read();
            using (var context = AuthContextManage.Context)
            {
                var server = context.Servers.FirstOrDefault(x => x.Id == serverId);
                if (server == null) return;
                session.Send(new ClientServerConnectSend(server.Address, server.Port));
            }
        }
    }
}