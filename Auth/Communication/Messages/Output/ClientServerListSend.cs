﻿using System.Collections.Generic;
using System.Linq;
using Commons.Utils;
using Database.Models;
using Networking;
using Networking.Messages;

namespace Center.Communication.Messages.Output
{
    public class ClientServerListSend : OutMessage
    {
        public ClientServerListSend(ICollection<ServerModel> servers, ICollection<UserCharacterModel> characters) : base(Opcodes.ClientServerListSend)
        {
            WriteInt8(0);
            WriteInt8((byte) servers.Count);
            foreach (var server in servers)
            {
                WriteInt16((short) server.Id);
                WriteInt16((short) server.Port);
                WriteAsciiString(server.Name);
                WriteAsciiString(server.Address);

                WriteInt32(server.Maintenance ? 0 : ServerStatus.Get(server.Count, server.Limit));
                WriteInt16((short) server.Count);
                WriteInt16(0);

                WriteInt8((byte) characters.Count(x => x.ServerId == server.Id)); // characters count
            }
        }
    }
}