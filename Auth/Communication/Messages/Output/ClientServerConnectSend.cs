﻿using System;
using System.Text;
using Networking;
using Networking.Messages;

namespace Center.Communication.Messages.Output
{
    public class ClientServerConnectSend : OutMessage
    {
        public ClientServerConnectSend(string host, int port) : base(Opcodes.ClientServerConnectSend)
        {
            Console.WriteLine($"{host}:{port}");
            WriteAsciiString(host);
            WriteUInt16((ushort) port);
        }
    }
}