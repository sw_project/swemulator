﻿using System.Text;
using Networking;
using Networking.Messages;

namespace Center.Communication.Messages.Output
{
    public class ClientLoginResult : OutMessage
    {
        public ClientLoginResult(uint accountId = 0,
            byte unknown4 = 0,
            string mac = "",
            string errorMessage = "",
            uint errorCode = 0,
            byte unknown820 = 0,
            string unknown822 = "",
            ulong sessionKey = 0,
            byte unknown858 = 0,
            ushort unknown85A = 0,
            byte unknown85C = 0,
            byte unknown85D = 0,
            byte unknown85E = 0) : base(Opcodes.ClientLoginResult)
        {
            WriteUInt32(accountId);
            WriteInt8(unknown4);
            Write(Encoding.UTF8.GetBytes(mac));
            WriteUTF8String(errorMessage);
            WriteUInt32(errorCode);
            WriteInt8(unknown820);
            WriteUTF8String(unknown822);
            WriteUInt64(sessionKey);
            WriteInt8(unknown858);
            WriteUInt16(unknown85A);
            WriteInt8(unknown85C);
            WriteInt8(unknown85D);
            WriteInt8(unknown85E);
        }
    }
}