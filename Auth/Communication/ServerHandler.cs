﻿using System;
using System.IO;
using Commons.Logging;
using Networking;
using Networking.Handler;
using Networking.Sessions;

namespace Center.Communication
{
    public class ServerHandler : IServerHandler
    {
        private static readonly Logger Logger = LogManager.Instance(LoggerLocation.Center, "ServerHandler");

        public void ChannelActive(Session session)
        {
            Logger.Info($"new connection {session.Address}");
        }

        public void ChannelInactive(Session session)
        {
            Logger.Info($"lost connection {session.Address}");
        }

        public void ChannelRead(Session session, ushort packetId, byte sender, byte[] buffer)
        {
            if (PacketProcessor.TryGetPacket(packetId, out var packetHandler))
                using (var ms = new MemoryStream(buffer))
                using (var br = new BinaryReader(ms))
                {
                    packetHandler.Dispatch(sender, br, session);
                }
        }
    }
}