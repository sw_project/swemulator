﻿namespace Character.Enums
{
    public enum Characters : byte
    {
        Haru,
        Erwin,
        Lily,
        Jin,
        Stella,
        Iris,
        Chii
    }
}