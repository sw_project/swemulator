﻿using Database.Models;
using Database.Models.Character;
using Networking;
using Networking.Messages;

namespace Character.Communication.Messages.Output
{
    public class ClientPlayGameSend : OutMessage
    {
        public ClientPlayGameSend(UserModel user, CharacterModel character) : base(Opcodes.ClientServerConnectSend)
        {
            WriteUInt32((uint)character.Id);   // Character id
            WriteUInt32((uint)user.Id);  // Session id

            Write(new byte[] {
                (byte) 0x02, (byte) 0x02, (byte) 0x02, (byte) 0x00, (byte) 0x31, (byte) 0x32, (byte) 0x20, (byte) 0x00, (byte) 0x31, (byte) 0x32, (byte) 0x20, (byte) 0x00,
                (byte) 0x92, (byte) 0xe1, (byte) 0x00, (byte) 0x00
            });

            WriteUInt32(152183);  // Unknown
            WriteUInt64(0);   // Unknown
            WriteAsciiString("127.0.0.1"); // Should get this from some setting
            WriteUInt16(9095); // Should get this from some setting
            WriteInt16(-1);
            WriteByteLoop(0x00, 36);
        }
    }
}