﻿using System;
using Networking;
using Networking.Messages;

namespace Character.Communication.Messages.Output
{
    public class ClientWorldCurDateSend : OutMessage
    {
        public ClientWorldCurDateSend(DateTime now) : base(Opcodes.ClientWorldCurDateSend)
        {
            WriteInt64((now.Ticks - new DateTime(1970, 1, 1, 0, 0, 0).Ticks) / 10000000);
            WriteInt16((short)now.Year);
            WriteInt16((short)now.Month);
            WriteInt16((short)now.Day);
            WriteInt16((short)now.Hour);
            WriteInt16((short)now.Minute);
            WriteInt16((short)now.Second);
            WriteBoolean(TimeZoneInfo.Utc.IsDaylightSavingTime(now));
        }
    }
}