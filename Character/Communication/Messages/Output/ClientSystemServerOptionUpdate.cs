﻿using Networking;
using Networking.Messages;

namespace Character.Communication.Messages.Output
{
    public class ClientSystemServerOptionUpdate : OutMessage
    {
        public ClientSystemServerOptionUpdate() : base(Opcodes.ClientSystemServerOptionUpdate)
        {
            WriteBoolean(true);
            WriteBoolean(false); // Enable secondary password
            WriteBoolean(true);
            WriteBoolean(false);
            WriteBoolean(true);
            WriteBoolean(true);
            WriteBoolean(false);
            WriteBoolean(false);
            WriteBoolean(true);
            WriteBoolean(true);
            WriteBoolean(false);
            WriteBoolean(true);
            WriteBoolean(false);
            WriteBoolean(false);
        }
    }
}