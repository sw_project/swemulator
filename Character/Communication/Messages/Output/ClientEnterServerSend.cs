﻿using Networking;
using Networking.Messages;

namespace Character.Communication.Messages.Output
{
    public class ClientEnterServerSend : OutMessage
    {
        public ClientEnterServerSend(int result, int userId) : base(Opcodes.ClientEnterServerSend)
        {
            WriteInt8((byte)result);
            WriteInt32(userId);
        }
    }
}