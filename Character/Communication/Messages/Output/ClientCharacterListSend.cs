﻿using System.Collections.Generic;
using Database.Models.Character;
using Networking;
using Networking.Messages;

namespace Character.Communication.Messages.Output
{
    public class ClientCharacterListSend : OutMessage
    {
        public ClientCharacterListSend(List<CharacterModel> characters) : base(Opcodes.ClientCharacterListSend)
        {
            WriteInt8((byte) characters.Count);
            foreach (var character in characters)
            {
                WriteInt32(character.Id);
                WriteUnicodeString(character.Name);
                WriteSInt8((sbyte) character.Type);
                WriteSInt8(0);
                WriteInt32(0);
                WriteUInt16((ushort) character.HairStyle);
                WriteUInt16((ushort) character.HairColor);
                WriteUInt16((ushort) character.EyeColor);
                WriteUInt16((ushort)character.SkinColor);
                WriteUInt16(0); // equipped hair style
                WriteUInt16(0); // equipped hair color
                WriteUInt16(0); // equipped eye color
                WriteUInt16(0); // equipped skin color
                WriteUInt16((ushort) character.Level);
                WriteByteLoop(0, 10);
                
                
                WriteInt32(character.WeaponId); // weapon id
                WriteSInt8(0);
                WriteInt32(-1);
                
                for (var e = 0; e < 13; e++)
                {
                    WriteInt32(-1); // ?
                    WriteInt32(-1); // ?
                    WriteInt32(-1); // Equipped item id
                    WriteUInt32(0);

                    WriteInt32(-1);
                    WriteInt32(-1);
                    WriteInt32(-1);
                    WriteUInt32(0);
                }

                WriteByteLoop(0, 62);
                WriteSingle(1f);
                WriteSingle(1f);
                WriteByteLoop(0, 20);

                WriteSInt8((sbyte) (character.Slot));
            }
            WriteUInt32(1); // character id selected
            WriteByteLoop(0, 10);
        }
    }
}