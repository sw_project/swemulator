﻿using System.IO;
using System.Linq;
using Character.Communication.Messages.Output;
using Database;
using Networking;
using Networking.Attributes;
using Networking.Crypt;
using Networking.Handler;
using Networking.Sessions;

namespace Character.Communication.Messages.Input
{
    [CodeIn(Opcodes.ClientSelectCharacterReq)]
    public class ClientSelectCharacterReq : IPacketHandler
    {
        public void Dispatch(byte sender, BinaryReader binaryReader, Session session)
        {
            int charId = binaryReader.ReadInt32();

            using (var charContext = CharacterContextManage.Context)
            {
                var character = charContext.Characters.Where(x => x.Id == charId).FirstOrDefault();
                if (character == null) return;

                var userModel = session.Model;

                using (var authContext = AuthContextManage.Context)
                {
                    userModel.SessionKey = Cryptography.GenerateSessionKey();
                    authContext.Update(userModel);
                }

                session.Send(new ClientPlayGameSend(userModel, character));
            }
        }
    }
}