﻿using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Character.Communication.Messages.Output;
using Database;
using Database.Models;
using Database.Models.Character;
using Networking;
using Networking.Attributes;
using Networking.Extensions;
using Networking.Handler;
using Networking.Sessions;

namespace Character.Communication.Messages.Input
{
    [CodeIn(Opcodes.ClientCreateCharacterReq)]
    public class ClientCreateCharacterReq : IPacketHandler
    {
        public void Dispatch(byte sender, BinaryReader binaryReader, Session session)
        {
            binaryReader.ReadInt32(); // unknown
            var name = binaryReader.ReadUnicodeString();
            var type = binaryReader.ReadInt16();

            binaryReader.ReadInt32(); // unknown

            var hairStyle = binaryReader.ReadInt16();
            var hairColor = binaryReader.ReadInt16();
            var eyeColor = binaryReader.ReadInt16();
            var skinColor = binaryReader.ReadInt16();

            binaryReader.ReadInt32();
            binaryReader.ReadInt32();
            var slot = binaryReader.ReadByte();
            if (name.Length < 2 || name.Length > 12) return;
            if (type < 1 || type > 7) return;
            if (slot > 7) return;
            var userModel = session.Model;

            if (CharacterContextManage.Context.Characters.Any(x => x.UserId == userModel.Id && x.Slot == slot)) return;
            name = Regex.Replace(name, "[^A-Za-z0-9]", "");
            if (CharacterContextManage.Context.Characters.Any(x => x.Name == name))
            {
                session.Send(new ClientCreateCharacter(0));
                return;
            }

            using (var context = CharacterContextManage.Context)
            {
                var character = new CharacterModel
                {
                    UserId = userModel.Id,
                    Name = name,
                    Type = type,
                    HairStyle = hairStyle,
                    HairColor = hairColor,
                    EyeColor = eyeColor,
                    SkinColor = skinColor,
                    Level = 1,
                    WeaponId = 110011301 + (1000000 * type),
                    Slot = slot,
                    GuildName = ""
                };

                context.Characters.Add(character);

                context.SaveChanges();

                AuthContextManage.Context.UserCharacters.Add(new UserCharacterModel
                {
                    Id = userModel.Id,
                    CharacterId = character.Id,
                    Slot = slot,
                    ServerId = 1,
                });
                AuthContextManage.Context.SaveChanges();

                var characters = context.Characters.Where(x => x.UserId == userModel.Id).ToList();
                session.Send(new ClientCharacterListSend(characters));
                session.Send(new ClientCreateCharacter(1));
            }
        }
    }
}