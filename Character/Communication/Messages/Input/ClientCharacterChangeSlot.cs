﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Character.Communication.Messages.Output;
using Database;
using Database.Models.Character;
using Networking;
using Networking.Attributes;
using Networking.Handler;
using Networking.Sessions;

namespace Character.Communication.Messages.Input
{
    [CodeIn(Opcodes.ClientCharacterChangeSlot)]
    public class ClientCharacterChangeSlot : IPacketHandler
    {
        public void Dispatch(byte sender, BinaryReader binaryReader, Session session)
        {
            binaryReader.ReadInt64(); // 1st padding
            binaryReader.ReadInt64(); // 2nd padding
            int charId = binaryReader.ReadByte(); // character id
            int slot = binaryReader.ReadByte(); // slot

            var userId = session.Model.Id;
            using (var context = CharacterContextManage.Context)
            {
                var character = context.Characters.Where(x => x.UserId == userId).Where(x => x.Id == charId).FirstOrDefault();
                if (character != null)
                {
                    character.Slot = slot;
                    context.Characters.Update(character);
                    context.SaveChanges();

                    var userModel = session.Model;
                    List<CharacterModel> characters = context.Characters.Where(x => x.UserId == userModel.Id).ToList();
                    session.Send(new ClientCharacterListSend(characters));
                    session.Send(new ClientSystemServerOptionUpdate());
                }
            }
        }
    }
}