﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Character.Communication.Messages.Output;
using Database;
using Database.Models.Character;
using Networking;
using Networking.Attributes;
using Networking.Handler;
using Networking.Sessions;

namespace Character.Communication.Messages.Input
{
    [CodeIn(Opcodes.ClientCharacterListReq)]
    public class ClientCharacterListReq : IPacketHandler
    {
        public void Dispatch(byte sender, BinaryReader binaryReader, Session session)
        {
            Console.WriteLine(binaryReader.ReadInt64());
            var userModel = session.Model;
            using (var context = CharacterContextManage.Context)
            {
                List<CharacterModel> characters = context.Characters.Where(x => x.UserId == userModel.Id).ToList();
                session.Send(new ClientCharacterListSend(characters));
            }
        }
    }
}