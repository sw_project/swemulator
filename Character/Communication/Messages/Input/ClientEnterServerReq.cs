﻿using System;
using System.IO;
using System.Linq;
using Character.Communication.Messages.Output;
using Database;
using Networking;
using Networking.Attributes;
using Networking.Extensions;
using Networking.Handler;
using Networking.Sessions;

namespace Character.Communication.Messages.Input
{
    [CodeIn(Opcodes.ClientEnterServerReq)]
    public class ClientEnterServerReq : IPacketHandler
    {
        public void Dispatch(byte sender, BinaryReader binaryReader, Session session)
        {
            var userId = binaryReader.ReadInt32();
            var serverId = binaryReader.ReadInt16();
            var sessionKey = binaryReader.ReadUInt64();
            var unknown2 = binaryReader.ReadByte();
            using (var context = AuthContextManage.Context)
            {
                var userModel = context.Users.FirstOrDefault(x => x.Id == userId);
                if (userModel == null || userModel.SessionKey != sessionKey)
                {
                    session.Disconnect();
                    return;
                }
                var serverModel = context.Servers.FirstOrDefault(x => x.Id == serverId);
                if (serverModel == null) return;

                session.Model = userModel;

                session.Send(new ClientEnterServerSend(0, userId));
                session.Send(new ClientWorldCurDateSend(DateTime.UtcNow));
            }
        }
    }
}