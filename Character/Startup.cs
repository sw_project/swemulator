﻿using System.Threading.Tasks;
using Character.Communication;
using Commons.Json;
using Commons.Json.Server;
using Commons.Logging;
using Database;
using Networking;
using Networking.Crypt;

namespace Character
{
    internal class Startup
    {
        private static readonly Logger Logger = LogManager.Instance(LoggerLocation.Center, "Startup");
        private static NetworkingSettings NetworkingSettings { get; set; }
        private static DatabaseSettings DatabaseSettings { get; set; }

        private static void Main(string[] args)
        {
            Logger.Info("Starting character server...");
            NetworkingSettings = new DeserializeFile<NetworkingSettings>().Build(@"data\networking.json");
            DatabaseSettings = new DeserializeFile<DatabaseSettings>().Build(@"data\database.json");
            Cryptography.Load("data/key_table");
            PacketProcessor.Initialize();
            AuthContextManage.Build(DatabaseSettings.Auth);
            CharacterContextManage.Build(DatabaseSettings.Character);
            AuthContextManage.Migrate();
            CharacterContextManage.Migrate();
            RunAsync().GetAwaiter().GetResult();
        }

        private static async Task RunAsync()
        {
            var bootstrap = new Bootstrap();
            await bootstrap.Bind(new ServerHandler(), NetworkingSettings);
            await Task.Delay(-1);
        }
    }
}