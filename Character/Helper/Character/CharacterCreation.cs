﻿namespace Character.Helper.Character
{
    public class CharacterCreation
    {
        public byte HairStyleA { get; set; }
        public byte HairStyleB { get; set; }
        public byte HairStyleC { get; set; }

        // Hair Colors
        public byte HairColorA { get; set; }
        public byte HairColorB { get; set; }
        public byte HairColorC { get; set; }

        // Eye Colors
        public byte EyeColorA { get; set; }
        public byte EyeColorB { get; set; }
        public byte EyeColorC { get; set; }

        // Skin Colors
        public byte SkinColorA { get; set; }
        public byte SkinColorB { get; set; }
        public byte SkinColorC { get; set; }

        // Clothing Style
        public byte ClothingStyleA { get; set; }
        public byte ClothingStyleB { get; set; }
        public byte ClothingStyleC { get; set; }
        public byte ClothingStyleD { get; set; }
    }
}