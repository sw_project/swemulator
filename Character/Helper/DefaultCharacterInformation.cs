﻿using System.Collections.Generic;
using Character.Enums;
using Character.Helper.Character;

namespace Character.Helper
{
    public class DefaultCharacterInformation
    {
        public Dictionary<Characters, CharacterCreation> Characters = new Dictionary<Characters, CharacterCreation>()
        {
            {
                Enums.Characters.Haru, new CharacterCreation()
                {
                    HairStyleA = 0x4D, HairStyleB = 0x4E, HairStyleC = 0x4F,
                    HairColorA = 0x35, HairColorB = 0x36, HairColorC = 0x37,
                    EyeColorA = 0xED, EyeColorB = 0xEE, EyeColorC = 0xEF,
                    SkinColorA = 0x1D, SkinColorB = 0x1E, SkinColorC = 0x1F,
                    ClothingStyleA = 0x6F, ClothingStyleB = 0x79, ClothingStyleC = 0x83, ClothingStyleD = 0x8D
                }
            },
            {
                Enums.Characters.Erwin, new CharacterCreation()
                {
                    HairStyleA = 0xB1, HairStyleB = 0xB2, HairStyleC = 0xB3,
                    HairColorA = 0x99, HairColorB = 0x9A, HairColorC = 0x9B,
                    EyeColorA = 0x51, EyeColorB = 0x52, EyeColorC = 0x53,
                    SkinColorA = 0x81, SkinColorB = 0x92, SkinColorC = 0x83,
                    ClothingStyleA = 0xD3, ClothingStyleB = 0xDD, ClothingStyleC = 0xE7, ClothingStyleD = 0xF1
                }
            },
            {
                Enums.Characters.Lily, new CharacterCreation()
                {
                    HairStyleA = 0x15,  HairStyleB = 0x16, HairStyleC = 0x17,
                    HairColorA = 0xFD, HairColorB = 0xFE, HairColorC = 0xFF,
                    EyeColorA = 0xB5, EyeColorB = 0xB6, EyeColorC = 0xB7,
                    SkinColorA = 0xE5, SkinColorB = 0xE6, SkinColorC = 0xE7,
                    ClothingStyleA = 0x37, ClothingStyleB = 0x41, ClothingStyleC = 0x4B, ClothingStyleD = 0x55
                }
            },
            {
                Enums.Characters.Jin, new CharacterCreation()
                {
                    HairStyleA = 0x79, HairStyleB = 0x7A, HairStyleC = 0x7B,
                    HairColorA = 0x61, HairColorB = 0x62, HairColorC = 0x63,
                    EyeColorA = 0x19, EyeColorB = 0x1A, EyeColorC = 0x1B,
                    SkinColorA = 0x49, SkinColorB = 0x4A, SkinColorC = 0x4B,
                    ClothingStyleA = 0x9B, ClothingStyleB = 0xA5, ClothingStyleC = 0xAF, ClothingStyleD = 0xB9
                }
            },
            {
                Enums.Characters.Stella, new CharacterCreation()
                {
                    HairStyleA = 0xDD, HairStyleB = 0xDE, HairStyleC = 0xDF,
                    HairColorA = 0xC5, HairColorB = 0xC6, HairColorC = 0xC7,
                    EyeColorA = 0x7D, EyeColorB = 0x7E, EyeColorC = 0x7F,
                    SkinColorA = 0xAD, SkinColorB = 0xAE, SkinColorC = 0xAF,
                    ClothingStyleA = 0xFF, ClothingStyleB = 0x09, ClothingStyleC = 0x13, ClothingStyleD = 0x1D
                }
            },
            {
                Enums.Characters.Iris, new CharacterCreation()
                {
                    HairStyleA = 0x41, HairStyleB = 0x42, HairStyleC = 0x43,
                    HairColorA = 0x29, HairColorB = 0x2A, HairColorC = 0x2B,
                    EyeColorA = 0xE1, EyeColorB = 0xE2, EyeColorC = 0xE3,
                    SkinColorA = 0x11, SkinColorB = 0x12, SkinColorC = 0x13,
                    ClothingStyleA = 0x63, ClothingStyleB = 0x6D, ClothingStyleC = 0x77, ClothingStyleD = 0x81
                }
            },
            {
                Enums.Characters.Chii, new CharacterCreation()
                {
                    HairStyleA = 0xA5,
                    HairColorA = 0x8D, HairColorB = 0x8E, HairColorC = 0x8F,
                    EyeColorA = 0x45, EyeColorB = 0x46, EyeColorC = 0x47,
                    SkinColorA = 0x75, SkinColorB = 0x76, SkinColorC = 0x77,
                    ClothingStyleA = 0xC7
                }
            }
        };

        public bool Get(Characters character, out CharacterCreation data)
        {
            return Characters.TryGetValue(character, out data);
        }
    }
}