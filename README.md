[![Soul Worker](https://wiki.soulworkerhq.com/images/b/b6/Soul-worker.jpg)](http://soulworker.game.onstove.com/)
# Soulworker ![Discord](https://img.shields.io/discord/633351503926657034)

A server emulator for the Soulworker MMO Game.


## Installation

Compile the entire solution with Visual Studio(Recommended is VS 2019) and set the data folder configurations as needed.

## Usage

You need the KR client to work. To start the client on the server, first make sure it is running and then:

```
SoulWorker.exe Live/127.0.0.1/10000 SkipSGAuthen:yes
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.