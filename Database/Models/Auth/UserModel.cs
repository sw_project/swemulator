﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Database.Models
{
    public class UserModel
    {
        [Key]
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string LastMac { get; set; }
        public string LastAddress { get; set; }
        public DateTime DateCreated { get; set; }
        public ulong SessionKey { get; set; }
        public HashSet<UserCharacterModel> Characters { get; set; } = new HashSet<UserCharacterModel>();
    }
}