﻿
using System.ComponentModel.DataAnnotations;

namespace Database.Models
{
    public class UserCharacterModel
    {
        [Key]
        public int Id { get; set; }
        public int CharacterId { get; set; }
        public int ServerId { get; set; }
        public int Slot { get; set; }
        public ServerModel Server { get; set; }
    }
}