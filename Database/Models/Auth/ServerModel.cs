﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Database.Models
{
    public class ServerModel
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public int Port { get; set; }

        public int Count { get; set; }

        public int Limit { get; set; }
        public bool Maintenance { get; set; }
        public HashSet<UserCharacterModel> Characters { get; set; } = new HashSet<UserCharacterModel>();
    }
}