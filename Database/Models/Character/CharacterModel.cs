﻿
using System;
using System.ComponentModel.DataAnnotations;

namespace Database.Models.Character
{
    public class CharacterModel
    {
        [Key]
        public int Id { get; set; }

        public int UserId { get; set; }

        public string Name { get; set; }

        public int Type { get; set; }

        public int HairStyle { get; set; }

        public int HairColor { get; set; }

        public int EyeColor { get; set; }

        public int SkinColor { get; set; }

        public int Level { get; set; }

        public int WeaponId { get; set; }

        public int TitleA { get; set; }

        public int TitleB { get; set; }

        public int GuildId { get; set; }

        public string GuildName { get; set; }

        public int HP { get; set; }

        public int HPBase { get; set; }

        public int Energy { get; set; }

        public int EnergyExtra { get; set; }

        public int EXP { get; set; }

        public int Zenny { get; set; }

        public int BP { get; set; }

        public int Ether { get; set; }

        public int Map { get; set; }

        public int X { get; set; }

        public int Y { get; set; }

        public int Z { get; set; }

        public int Rotation { get; set; }

        public int Slot { get; set; }

        public DateTime DateCreated { get; set; }
    }
}