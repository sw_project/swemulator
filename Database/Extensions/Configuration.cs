﻿using System;
using Database.Configuration;
using Microsoft.EntityFrameworkCore;

namespace Database.Extensions
{
    public static class Configuration
    {
        public static void UseConfiguration(this DbContextOptionsBuilder optionsBuilder, IDatabaseConfiguration configuration, DatabaseType type)
        {
            var connectionString = configuration.GetConnectionString(type);
            switch (connectionString.Provider)
            {
                case DatabaseProvider.MySql:
                    optionsBuilder.UseMySql(connectionString.ConnectionString);
                    break;
                default:
                    throw new NotSupportedException();
            }
        }
    }
}